var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuvw~",
  1: "bcefmopstv",
  2: "bg",
  3: "bcefmopstv",
  4: "abcdefgilmorstuv~",
  5: "abcdefghilmnprstvw",
  6: "bv",
  7: "s",
  8: "bs",
  9: "mot"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros"
};

